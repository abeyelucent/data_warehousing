var parm1 = process.argv[2];
var parm2 = process.argv[3];
var DataLake = require('./datalake');
var Promise = require('bluebird');
var dl = new DataLake()
if (dl[parm1]) {
    dl[parm1](parm2)
    .then(()=> {
        if(parm2 && parm1 !='updateRowCount' && parm1 != 'dropTable') {
            return dl.updateRowCount(parm2)
        } else {
            return
        }
    })
    .then(process.exit).catch((err)=> {
    console.error(err);
    process.exit();
})
} else {
    console.error(parm1 + ' is not a valid action. Exiting')
    process.exit();
}
