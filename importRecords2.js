const EventEmitter = require('events');
var Promise = require('bluebird')
require('./encryption.js')
var request = require('request-promise')
var SNUtils = require('./lib/utils/SNUtils')
var importAll = require('./lib/importClasses/importAll');
var db = require('./lib/utils/DBUtils');
var DBUtils = new db();
var requestFactory = require('./lib/utils/requestFactory')
var importAll = new importAll()
var table = 'sys_dictionary'
class MyEmitter extends EventEmitter {}
const recordEvents = new EventEmitter();
var pageIterator;
var offset = 0;
var etl = require('./lib/utils/ETLUtils.js')

var rowlimit = config.servicenow.rowlimit
var rowcount;
var schema;
Promise.all([SNUtils.getTableSchemaObject(table), importAll.query(table, DBUtils.knex), SNUtils.getTableFilter(table)]).then((val) => {
    schema = val[0]
    pageIterator = requestFactory.requestFunction(table, val[1])
    recordEvents.emit('retrieve')

})
recordEvents.on('retrieve', () => {
    console.log('retrieving records ' + (offset+1) + ' to ' + (offset+rowlimit))
    request(pageIterator(offset)).then((res) => {
        rowcount = res.headers['x-total-count']
        offset = offset+rowlimit
        recordEvents.emit('transform', res.body.result)
        if(offset>=rowcount)
        process.exit()
        recordEvents.emit('retrieve')
    })
});
// console.log(config);
recordEvents.on('transform', (records) => {
    console.log('transforming')
    var tt = records.map(function(record) {
        return etl.mapRecord(record, schema)
    })
    recordEvents.emit('import',tt)
});
recordEvents.on('import', (records) => {
    importAll.dbFunction(table,records,DBUtils.knex).then(() => {
        recordEvents.emit('records_imported');
    })
});
recordEvents.on('records_imported',() => {
    console.log('records imported')
})
// recordEvents.on('import_complete', () => {
//   console.log('an event occurred!');
// });
