var express = require('express');
var bodyParser = require('body-parser');


var app = express();
var DataLake = require('./DataLake')
var request = require('request-promise')
var dl = new DataLake();
app.use(bodyParser.json());
app.get('/', function(req, res) {
    res.send(Object.getOwnPropertyNames(DataLake.prototype))
})
app.post('/:action', (req, res) => {
    console.log(req.body)
    dl[req.params.action](req.body.table)
    console.log(req.params.action)
    res.send('success')
})
app.listen(8080, function() {
})
