var sjcl = require('sjcl')
var fs = require('fs')
var encryptprepend = 'encrypted:'
// var salt = sjcl.codec.utf8String.fromBits(tt)

// var configPath = './config.json'
// console.log(getSalt())
// console.log(getDEK())
// var tt = new sjcl.codec.utf8String.toBits('sdfkj')
// console.log(process.argv)
var config_path = __dirname+'\\config.json'
config = require(config_path)
var snp = config.servicenow.pass;
var dbp = config.db.pass;

if (isEncrypted(config.servicenow.pass)) {
    config.servicenow.pass = decrypt(config.servicenow.pass)
} else {
    var encryptsn = true
}
if (isEncrypted(config.db.pass)) {
    config.db.pass = decrypt(config.db.pass)
} else {
    var encryptdb = true
}
if (encryptsn || encryptdb) {
    var servicenow = Object.assign({}, config.servicenow);
    var db = Object.assign({}, config.db);
    var encconfig = {}
    encconfig.servicenow = servicenow;
    encconfig.db = db;
    encconfig.servicenow.pass = encrypt(encconfig.servicenow.pass);
    encconfig.db.pass = encrypt(encconfig.db.pass);
    fs.writeFileSync(config_path, JSON.stringify(encconfig, null, 4))
}

function isEncrypted(pass) {
    return pass.startsWith(encryptprepend)
}

function encrypt(pass) {
    var ciphertext = sjcl.encrypt(getDEK(), pass, {
        mode: "gcm"
    })
    return encryptprepend + convertFromCipherText(ciphertext)
}

function decrypt(encrypted) {
    var enc = encrypted.substring(encryptprepend.length, encrypted.length)
    var ciphertext = convertToCipherText(enc)
    return sjcl.decrypt(getDEK(), ciphertext);
}

function convertFromCipherText(ciphertext) {
    var ct = JSON.stringify(ciphertext);
    return new Buffer(ct).toString('base64')
}

function convertToCipherText(str) {
    return JSON.parse(Buffer.from(str, 'base64').toString('utf8'))
    // return Buffer(str).toString('utf8')
}


// if(fs.existsSync(configPath) && !fs.existsSync(encryptPath)){
//     var config_string = fs.readFileSync(configPath).toString('utf8')
//     config = JSON.parse(config_string);
//     var encrypted = encrypt(config_string,getKEK())
//     fs.writeFileSync(encryptPath,encrypted)
//     fs.unlinkSync(configPath);
// } else if(fs.existsSync(encryptPath)) {
//     var config_enc = fs.readFileSync(encryptPath).toString('utf8');
//     var config_string = decrypt(config_enc,getKEK());
//     config = JSON.parse(config_string)
// }
// console.log(derivedKey)
function getDEK() {
    return sjcl.hash.sha256.hash(getKEK())
}

function getKEK() {
    return sjcl.misc.pbkdf2(getPass(), getSalt(), 10000, 500, sjcl.hash.sha512)
    // return pbkdf2.pbkdf2Sync(pass, salt, 10000, 512, 'sha512').toString('hex')
}

function getPass() {
    const os = require('os');
    var ni = os.networkInterfaces();
    var fi = ni[Object.keys(ni)[0]][0]
    var name = os.hostname()
    var ip = fi.address
    var mac = fi.mac
    return name + ip + mac
}

function getSalt() {
    var salt_path = __dirname+'\\salt'
    var salt_exists = fs.existsSync(salt_path);
    if (salt_exists) {
        return JSON.parse(fs.readFileSync(salt_path, 'utf8'))
    } else {
        var salt = sjcl.random.randomWords(8);
        fs.writeFileSync(salt_path, JSON.stringify(salt))
        return salt
    }
}
