var request = require('request-promise');
var DBUtils = require('./utils/DBUtils')
var SNUtils = require('./utils/SNUtils')
var etl = require('./utils/ETLUtils.js')
var requestFactory = require('./utils/requestFactory')
var Promise = require('bluebird');
var rowlimit = config.servicenow.rowlimit

module.exports = importRecords;

// var createdclass = require('./importAll');
// importRecords('cmdb_ci', new createdclass())

function importRecords(table, importClass,knex) {
    console.log('Importing Table: ' + table)
    var schema;
    var pageIterator;
    var table = table;
    var schema;
    var rowcount = 0;
    var importedrows = 0;
    var totalrowcount;
    var dbretry = 0;
    var reqretry = 0;
    var reqretrycount = config.servicenow.retryattempts;
    var dbretrycount = config.db.retryattempts;
    return Promise.all([SNUtils.getTableSchemaObject(table), importClass.query(table, knex),SNUtils.getTableFilter(table)]).then((values) => {
        schema = values[0];
        var query = values[1];
        if(values[2]) {
            query = query + '^'+ values[2]
        }
        pageIterator = requestFactory.requestFunction(table,query)
        return getAndImport()
    })


    function getAndImport(offset) {
        return getRecords(offset || 0).then(function(records) {
            rowcount = rowcount + rowlimit;
            var rows = records.length;
            if (records.length) {
                return dbLoad(records)
            } else {
                console.log('Import complete')
                // process.exit(0);
            }
        })
    }

    function getRecords(offset) {
        return request(pageIterator(offset)).then((response) => {
            reqretry = 0
            // console.log(response)
            totalrowcount = response.headers['x-total-count']
            var records = response.body.result;
            return records.map(function(record) {
                return etl.mapRecord(record, schema)
            })
        }).catch((val) => {
            console.log(val)
            console.log('request error')
            if (reqretry < reqretrycount) {
                reqretry++
                console.log('Request failed - retrying attempt ' + reqretry + ' of ' + reqretrycount)
                return getRecords(offset);
            } else {
                console.error(val)
                console.error('Request: Retry Attempts failed, exiting');
                process.exit()
            }
        })
    }

    function dbLoad(records) {
        return importClass.dbFunction(table, records, knex).then((val) => {
            dbretry = 0
            importedrows = importedrows + records.length
            console.log(table + ': ' + importedrows + ' of ' + totalrowcount + ' imported')
            if(importedrows < totalrowcount) {
                return getAndImport(importedrows)
            } else {
                console.log('Import complete')
            }
        }).catch((val) => {
            console.log(val)
            if (dbretry < dbretrycount) {
                dbretry++
                console.log('Import failed - retrying attempt + ' + dbretry + ' of ' + dbretrycount)
                return dbLoad(records)
            } else {
                console.error('KNEX: Retry Attempts failed, exiting');
            }
        })
    }
}
