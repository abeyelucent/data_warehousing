var SNUtils = require('./utils/SNUtils')
const spawn = require('child_process').spawn;
var EventEmitter = require('events').EventEmitter
var pool = 0
var maxpool = 10
// myEmitter.emit('event')
SNUtils.getImportTables().then(function(tables) {
    var currimporttable = 0
    var finished = 0
    var myEmitter = new EventEmitter();
    myEmitter.on('close', () => {
        if (currimporttable < tables.length) {
            executeImportTable(tables[currimporttable])
            currimporttable++
        }
        if(finished == tables.length) {
            process.exit()
        }
    })
    var tl = tables.length;
    for ( var x = 0; x < maxpool;x++){
        executeImportTable(tables[x])
        currimporttable++
    }

    function executeImportTable(table) {
        var tt = spawn('node', ['./runscript.js','importAllRecords',table.table])
        tt.stdout.on('data', (data) => {
            console.log(`${data}`);
        });

        tt.on('close', (code) => {
            finished++
            myEmitter.emit('close');
        });
        tt.on('error', (code) => {
            console.log('error');
        })
    }
})
