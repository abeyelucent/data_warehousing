var request = require('request-promise');
var requestFactory = require('./requestFactory')
var SNUtils = {
    getRowCount: function(table) {
        return SNUtils.getTableFilter(table).then((query)=>{
            return request(requestFactory.stats(table,query))
        })
        // return request(requestFactory.stats(table))
    },
    getImportTables: function() {
        return request(requestFactory.getTables).then(function(tables) {
            return tables;
        }).catch(function(err) {
            console.log('REQUEST ERROR: ' + err)
        })
    },
    getTableFilter: function(table) {
        return request(requestFactory.getTableFilter(table)).then((record) => {
            return record
        })
    },
    getTableSchema: function(table) {
        var opts = requestFactory.schema(table)
        return request(opts)
    },
    getTableSchemaObject: function(table) {
        return this.getTableSchema(table).then(function(schema) {
            var obj = {};
            schema.forEach(function(elem) {
                obj[elem.name] = elem;
            })
            return obj;
        })
    },
    getTableRecord: function(table) {
        return request(requestFactory.getTableRecord(table))
    },
    updateRowCount: function(table,counts) {
        return this.getTableRecord(table).then((sys_id)=> {
            var obj = requestFactory.updateTableRecord(sys_id);
            obj.body = counts
            return request(obj).then((val) => {
                return
            })
        })
    },
    getDeletedRecords: function(table,date) {
        return request(requestFactory.getDeletedRecords(table,date))
    },
    getMostRecentTransactionByAction(table,action) {
        return request(requestFactory.getMostRecentTransactionByAction(table,action))
    }
}
module.exports = SNUtils
