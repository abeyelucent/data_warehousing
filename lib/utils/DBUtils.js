// var config = require('../../config.json')
var etl = require('./ETLUtils')
var _ = require('lodash')
var Promise = require('bluebird')
class DBUtils {
    constructor() {
        this.knex = require('knex')({
            client: 'mssql',
            connection: {
                host: config.db.server || '127.0.0.1',
                user: config.db.user || 'root',
                password: config.db.pass || '',
                database: config.db.database || '',
                port: config.db.port || '1433',
                domain: config.db.domain || '',
                connectionTimeout: config.db.connectiontimeout || 120000,
                requestTimeout: config.db.requesttimeout || 120000,
                options: {
                    encrypt: true
                }

            },
        });
        this.SNUtils = require('./SNUtils')
        this.ETLUtils = require('./ETLUtils')
    }
    getTableSchema(table) {
        var raw = "SELECT * FROM " + config.db.database + ".INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + table + "'"
        return this.knex.schema.raw(raw)
    }
    incrementalDelete(table) {
        return Promise.props({
            importAllRecords: this.SNUtils.getMostRecentTransactionByAction(table, 'importAllRecords'),
            incrementalDelete: this.SNUtils.getMostRecentTransactionByAction(table, 'incrementalDelete')
        }).then((value) => {
            var curr_date = this.ETLUtils.dateForDelete(value)
            return this.SNUtils.getDeletedRecords(table, this.ETLUtils.dateForDelete(value)).then((val) => {
                console.log('Syncing ' + val.result.length + ' deleted records since ' + curr_date)
                return this.deleteRecords(table, val.result)
            })
        })
    }
    createTable(table) {
        console.log('creating table ' + table);
        return this.SNUtils.getTableSchema(table).then((elements) => {
            return this._createTable(table, elements).then(() => {
                return
            }).catch((val) => {
                console.log(val)
                console.log('table create fail')
            })
        })
    }
    syncTableSchema(table) {
        return Promise.all([this.getTableSchema(table), this.SNUtils.getTableSchema(table)]).then((val) => {
            var dbschema = val[0]
            var snschema = val[1]
            var missing = [];
            snschema.forEach((element) => {
                var match = _.find(dbschema, function(o) {
                    return o.COLUMN_NAME == element.name
                })
                if (!match) {
                    missing.push(element)
                }
            })
            console.log(missing)
            return this._alterTable(table, missing)
        })
    }
    _createTable(table, elements) {
        return this.knex.schema.createTableIfNotExists(table, function(kn) {
            elements.forEach(function(element) {
                if (parseInt(element.max_length) > 4000) element.max_length = 4000
                etl.mapField(kn, element, table)
            })
        })
    }
    _alterTable(table, elements) {
        return this.knex.schema.alterTable(table, function(kn) {
            elements.forEach(function(element) {
                if (parseInt(element.max_length) > 4000) element.max_length = 4000
                etl.mapField(kn, element, table)
            })
        })
    }
    dropAllTables() {
        var raw = ` DECLARE @sql NVARCHAR(max)=''

        SELECT @sql += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
        FROM   INFORMATION_SCHEMA.TABLES
        WHERE  TABLE_TYPE = 'BASE TABLE'

        Exec Sp_executesql @sql`
        return this.knex.schema.raw(raw).then(function(val) {
            console.log('Database cleared')
            return
        })
    }
    deleteAllRecords(table) {
        return this.knex(table).del().then(function(val) {
            console.log('Records deleted')
            return
        }).catch(function(val) {
            console.log('Error deleting records:\n' + val)
        })
    }
    deleteRecords(table, sids) {
        return this.knex(table).whereIn('sys_id', sids).del().then(function(val) {
            console.log('Records deleted');
            return
        }).catch(function(val) {
            console.log('Error delete records: \n' + val);
        })
    }
    getRowCount(table) {
            return this.knex(table).count().then(function(val) {
                return val[0]['']
            })
        }
        // dropAllTables: function(table, knex) {
        //     return getImportTables().then(function(table) {
        //         this.dropTable(table.table)
        //     })
        // },
    dropTable(table) {
        return this.knex.schema.dropTable(table).then(function(val) {
            console.log('table deleted')
            return;
        })
    }
    getLatestTime(table, field) {
        return this.knex(table).orderBy(field, 'desc').select(field).limit(1).then(function(val) {
            if (val.length) {
                return val[0][field].toISOString().replace('.000Z', '').replace('T', ' ')
            } else {
                return '1971-01-01 00:00:00'
            }
        })
    }
    createSchema() {
        console.log('Starting schema creation...')
        var knex = this.knex()
        return this.SNUtils.getImportTables().then((tables) => {
            console.log(tables.length + ' import tables received')
            var parr = [];
            var failcount = 0
            var successcount = 0
            var failedtables = []
            tables.forEach((table) => {
                parr.push(this.createTable(table.table))
            })
            return Promise.all(parr).then(function(val) {
                // console.log(successcount + ' out of ' + tables.length + ' tables created or already exist')
                console.log('Database schema creation complete');
                if (failedtables.length) {
                    console.log('The following tables failed to be created: ' + failedtables.join(','))
                }
                process.exit()
            })
        })
    }
}
module.exports = DBUtils
