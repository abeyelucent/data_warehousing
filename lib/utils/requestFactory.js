// var config = require('../../config.json')
var etl = require('./ETLUtils')
var xm = require('xml2js').parseString
var _ = require('lodash')
var auth = {
    user: config.servicenow.user,
    pass: config.servicenow.pass
}
module.exports = {
    requestFunction: function(table, query) {
        return function(offset) {
            return {
                url: config.servicenow.instance + 'api/now/table/' + table,
                // transform: etl.reqTransform,
                auth: auth,
                proxy: config.servicenow.proxy,
                qs: {
                    sysparm_query: query,
                    sysparm_limit: config.servicenow.rowlimit,
                    sysparm_offset: offset || 0
                },
                json: true,
                resolveWithFullResponse: true
            }
        }
    },
    getCreated: function(table, date) {
        return function(offset) {
            return {
                url: config.servicenow.instance + 'api/now/table/' + table,
                transform: etl.reqTransform,
                auth: auth,
                proxy: config.servicenow.proxy,
                qs: {
                    sysparm_query: 'ORDERBYsys_created_on^ORDERBYsys_id^sys_created_on>' + date,
                    sysparm_limit: config.servicenow.rowlimit,
                    sysparm_offset: offset || 0
                },
                json: true
            }
        }
    },
    schema: function(table) {
        return {
            url: config.servicenow.instance + table + '.do?SCHEMA',
            method: 'GET',
            transform: function(body) {
                return new Promise(function(resolve, reject) {
                    xm(body, function(err, res) {
                        if (res.error) {
                            console.log(res.error)
                            reject(res.error)
                        }
                        var elements = res[table].element.map(function(elem) {
                            return elem.$
                        })
                        resolve(_.orderBy(elements, "name"));
                    })
                })
            },
            auth: {
                user: config.servicenow.user,
                pass: config.servicenow.pass
            },
            proxy: config.servicenow.proxy,
            json: true,
        }
    },
    stats: function(table, query) {
        return {
            url: config.servicenow.instance + 'api/now/stats/' + table,
            auth: {
                user: config.servicenow.user,
                pass: config.servicenow.pass
            },
            transform: function(body) {
                return parseInt(body.result.stats.count)
            },
            proxy: config.servicenow.proxy,
            qs: {
                sysparm_count: true,
                sysparm_query: query || ''
            },
            json: true
        }
    },
    getTables: {
        url: config.servicenow.instance + 'api/now/table/u_ssis_import_table',
        method: 'GET',
        transform: function(body) {
            return body.result.map(function(val) {
                return {
                    table: val.u_table_name
                }
            })
        },
        proxy: config.servicenow.proxy,
        auth: {
            user: config.servicenow.user,
            pass: config.servicenow.pass
        },
        json: true,
        qs: {
            sysparm_query: 'u_active=true^ORDERBYu_table_name'
        }
    },
    getTableFilter: function(table) {
        return {
            url: config.servicenow.instance + 'api/now/table/u_ssis_import_table',
            method: 'GET',
            transform: (body) => {
                return body.result[0].u_filter
            },
            proxy: config.servicenow.proxy,
            auth: {
                user: config.servicenow.user,
                pass: config.servicenow.pass
            },
            json: true,
            qs: {
                sysparm_query: 'u_table_name=' + table,
                sysparm_limit: 1,
                sysparm_fields: 'u_filter'
            }
        }
    },
    getUpdated: function(table, created, updated) {
        return function(offset) {
            return {
                url: 'https://visamongoose.service-now.com/api/now/table/' + table,
                transform: etl.reqTransform,
                auth: auth,
                proxy: config.servicenow.proxy,
                qs: {
                    sysparm_query: 'ORDERBYsys_updated_on^ORDERBYsys_id^sys_updated_on>' + updated + '^sys_created_on<' + created,
                    sysparm_limit: config.servicenow.rowlimit,
                    sysparm_offset: offset || 0
                },
                json: true
            }
        }
    },
    fullImport: function(table) {
        var table = table;
        return function(offset) {
            return {
                url: config.servicenow.instance + 'api/now/table/' + table,
                transform: etl.reqTransform,
                auth: auth,
                proxy: config.servicenow.proxy,
                qs: {
                    sysparm_query: 'ORDERBYsys_created_on^ORDERBYsys_id',
                    sysparm_limit: config.servicenow.rowlimit,
                    sysparm_offset: offset
                },
                json: true
            }
        }
    },
    getTableExtensions: function(table) {
        var table = table;
        return function(offset) {
            return {
                url: config.servicenow.instance + 'api/visa2/gettableextensions/' + table,
                auth: auth,
                proxy: config.servicenow.proxy,
                json: true
            }
        }
    },
    incrementalDelete: function(tables) {
        var tables = tables;
        return function(offset) {
            return {
                url: config.servicenow.instance + 'api/now/table/sys_audit_delete',
                auth: auth,
                transform: etl.reqTransform,
                proxy: config.servicenow.proxy,
                qs: {
                    sysparm_query: 'sys_created_onONToday@javascript:gs.daysAgoStart(0)@javascript:gs.daysAgoEnd(0)^ORsys_created_onONYesterday@javascript:gs.daysAgoStart(1)@javascript:gs.daysAgoEnd(1)^tablenameIN' + tables,
                    sysparm_limit: config.servicenow.rowlimit,
                    sysparm_offset: offset || 0
                },
                json: true,
                transform2xxOnly: true
            }
        }
    },
    getTableRecord: function(table) {
        return {
            url: config.servicenow.instance + 'api/now/table/u_ssis_import_table',
            method: 'GET',
            transform: (body) => {
                console.log(body)
                return body.result[0].sys_id
            },
            auth: auth,
            proxy: config.servicenow.proxy,
            json: true,
            qs: {
                sysparm_query: 'u_table_name=' + table,
                sysparm_limit: 1,
                sysparm_fields: 'sys_id'
            },
            transform2xxOnly: true
        }
    },
    updateTableRecord: function(sys_id) {
        return {
            auth: auth,
            method: 'PATCH',
            url: config.servicenow.instance + 'api/now/table/u_ssis_import_table/' + sys_id,
            proxy: config.servicenow.proxy,
            auth: auth,
            json: true,
            transform2xxOnly: true
        }
    },
    getTableRecord: function(table) {
        return {
            url: config.servicenow.instance + 'api/now/table/u_ssis_import_table',
            method: 'GET',
            transform: (body) => {
                return body.result[0].sys_id
            },
            auth: auth,
            proxy: config.servicenow.proxy,
            json: true,
            qs: {
                sysparm_query: 'u_table_name=' + table,
                sysparm_limit: 1,
                sysparm_fields: 'sys_id'
            },
            transform2xxOnly: true
        }
    },
    getDeletedRecords: function(table, date) {
        return {
            url: config.servicenow.instance + 'api/visa2/getdeletedrecords/' + table,
            method: 'GET',
            transform: (body) => {
                // console.log(body)
                return body
                // return body.result[0].sys_id
            },
            auth: auth,
            proxy: config.servicenow.proxy,
            json: true,
            qs: {
                datetime: date
            },
            transform2xxOnly: true
        }
    },
    getMostRecentTransactionByAction: function(table,action) {
        return {
            url: config.servicenow.instance + 'api/now/table/u_dw_transactions',
            method: 'GET',
            transform: (body) => {
                // console.log(body)
                return body.result
                // return body.result[0].sys_id
            },
            auth: auth,
            proxy: config.servicenow.proxy,
            json: true,
            qs: {
                sysparm_query: 'ORDERBYDESCsys_created_on^u_administrative_action.u_command='+action+'^u_table.u_table_name='+table,
                sysparm_limit: 1
            },
            transform2xxOnly: true
        }
    }
}
