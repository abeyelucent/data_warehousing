module.exports = {
    dateForDelete: function(value) {
        var curr_date;
        if (value.incrementalDelete.length) {
            curr_date = value.incrementalDelete[0].sys_created_on
        } else if (value.importAllRecords.length) {
            curr_date = value.importAllRecords[0].sys_created_on
        } else {
            var d = new Date()
            d.setDate(d.getDate() - 7)
            var tt = d.toISOString().replace('T', ' ')
            curr_date = tt.substring(0, tt.length - 5)
        }
        return curr_date
    },
    reqTransform: function(body) {
        console.log('Received ' + body.result.length + ' records');
        return body.result
    },
    fieldMapping: function(element, table_name) {
        var func = this.fieldMap[element.internal_type]
        return this.typefunc(func)
    },

    mapField: function(kn, elem, table_name) {
        var func = this.fieldMapping(elem, table_name)
        if (!func) {
            console.log(elem);
        }
        func(kn, elem)
    },
    mapRecord: function(record, schema) {
        var row = {};
        var fields = Object.keys(record);
        var start = new Date('1753-01-01 00:00:00')
        // console.log('\n\n\n\n\n\n')
        fields.forEach(function(field) {
            // console.log(schema[field].max_length + ' - ' + record[field].length + ' - ' + field)
            if (schema[field].internal_type == 'glide_date_time') {
                if (new Date(record[field]) < start) {
                    record[field] = new Date().toISOString().replace('T', ' ').replace('Z', '');
                } else {
                    row[field] = record[field]
                }
            } else if (typeof record[field] == 'object' && record[field] != null) {
                if (record[field].value)
                    row[field] = record[field].value;
            } else if (record[field] != '' && record[field] != null) {
                row[field] = record[field];
            }
        })
        return row
    },
    typefunc: function(func) {
        var funcs = {
            "string": function(kn, element) {
                var l = parseInt(element.max_length) * 4
                var len;
                if (l == 0 || l >= 1000) {
                    len = 'max'
                } else if(element.choice_list == true || element.choice_list == "true"){
                    len = 'max'
                } else {
                    len = l
                }
                // if (l == 0) l = 4000;
                kn.specificType(element.name, 'nvarchar('+ len + ')')
            },
            "sys_id": function(kn, element) {
                var sys_id = kn.string(element.name, 32)
                sys_id.primary()
            },
            "integer": function(kn, element) {
                kn.integer(element.name)
            },
            "time": function(kn, element) {
                kn.timestamp(element.name)
            },
            "datetime": function(kn, element) {
                kn.dateTime(element.name)
            },
            "date": function(kn, element) {
                kn.date(element.name)
            },
            "reference": function(kn, element) {
                kn.string(element.name, element.reference_field_max_length)

            },
            "boolean": function(kn, element) {
                kn.boolean(element.name)
            },
            "float": function(kn, element) {
                kn.float(element.name)
            },
            "decimal": function(kn, element) {
                kn.decimal(element.name)
            }

        }
        return funcs[func]
    },
    fieldMap: {
        "glide_list": "string",
        "reference": "reference",
        "journal": "string",
        "string": "string",
        "domain_path": "string",
        "sys_class_name": "string",
        "variables": "string",
        "journal_list": "string",
        "user_input": "string",
        "domain_id": "string",
        "timer": "string",
        "journal_input": "string",
        "related_tags": "string",
        "boolean": "boolean",
        "glide_date_time": "datetime",
        "glide_duration": "time",
        "due_date": "datetime",
        "glide_date": "date",
        "GUID": "sys_id",
        "integer": "integer",
        "choice": "string",
        "conditions": "string",
        "ip_address": "string",
        "float": "float",
        "decimal": "decimal",
        "password": "string",
        "currency": "decimal",
        "price": "decimal",
        "decoration": "string",
        "composite_field": "string",
        "ph_number": "string",
        "user_roles": "string",
        "email": "string",
        "html": "string",
        "workflow": "string",
        "percent_complete": "decimal",
        "script_plain": "string",
        "table_name": "string",
        "user_image": "string",
        "multi_two_lines": "string",
        "url": "string",
        "wiki_text": "string",
        "template_value": "string",
        "translated_text": "string",
        "translated_html": "string",
        "slushbucket": "string",
        "String": "string",
        "image": "string",
        "color": "string",
        "data_structure": "string",
        "short_field_name": "string",
        "glide_time": "string",
        "document_id": "string",
        "field_name": "string",
        "short_table_name": "string",
        "script": "string",
        "documentation_field": "string",
        "translated_field": "string",
        "string_full_utf8": "string",
        "field_list": "string",
        "catalog_preview": "string",
        "char": "string",
        "day_of_week": "string"
    }
}
