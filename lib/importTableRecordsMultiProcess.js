var request = require('request-promise');
require('../encryption');
var table = process.argv[2]
var requestFactory = require('./utils/requestFactory')
var etl = require('./utils/ETLUtils')
var DBUtils = require('./utils/DBUtils')
db = new DBUtils()
// var knex = db.knex()
var SNUtils = require('./utils/SNUtils')
// var getTableSchema = SNUtils.getTableSchemaObject
var poollimit = 5
var schema;
var rowcount = 0
var recsperrequest = config.servicenow.rowlimit;
var pageIterator
var error = []
var importCreated = require('./importClasses/importCreated')
var ic = new importCreated(db);
ic.query(table, db.knex).then((query) => {
    SNUtils.getTableFilter(table).then((filter) => {
        if(filter) {
            query = query + '^' + filter
        }
        pageIterator = recordIterator(table, query);
        SNUtils.getTableSchemaObject(table).then(function(sch) {
            schema = sch
            var arr = []
            for (var x = 0; x < poollimit; x++) {
                arr.push(getRecords(rowcount))
                rowcount = rowcount + recsperrequest
            }
            // Promise.all(arr).then(() => {
            //     console.log('done')
            // })
        })
    })

})


function getRecords(offset) {
    console.log('Getting records ' + (offset + 1) + ' to ' + (offset + recsperrequest))
    var opts = pageIterator(offset)
    return request.get(opts).then((body) => {
        var records = body.map((record) => {
            return etl.mapRecord(record, schema)
        })
        if (records.length) {
            insertRecords(records, 5)
        } else {
            console.log('else')
            return 'success'
        }
    }).catch(function(err) {
        error.push(offset)
        console.log(err)
        console.log('There was a request error: retrying')
        getRecords(offset)
    })
}

function insertRecords(records, retrycount) {
    return db.knex.batchInsert(table, records, 10).then(() => {
        console.log('records inserted')
        var tt = getRecords(rowcount)
        rowcount = rowcount + recsperrequest
        return tt
    }).catch((err) => {
        if (retrycount > 0) {
            console.log(Object.keys(err))
            console.log('there was an insert error')
            insertRecords(records, retrycount - 1)
        } else {
            getRecords(rowcount);
            rowcount = rowcount + recsperrequest
        }
        //rowcount = rowcount + recsperrequest;
    })
}

function recordIterator(table, query) {
    var table = table;
    return function(offset) {
        return {
            url: config.servicenow.instance + 'api/now/table/' + table,
            transform: function(body) {
                console.log('Received ' + body.result.length + ' records');
                return body.result
            },
            auth: {
                user: config.servicenow.user,
                pass: config.servicenow.pass
            },
            proxy: config.servicenow.proxy,
            qs: {
                sysparm_query: query,
                sysparm_limit: recsperrequest,
                sysparm_offset: offset
            },
            json: true
        }
    }
}

process.on('exit', () => {
    console.log('failed: ' + error)
})
