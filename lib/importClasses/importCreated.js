
var getLatestTime = require('../utils/DBUtils').getLatestTime
var Promise = require('bluebird');
var requestFactory = require('../utils/requestFactory')
class importCreated {
    constructor(DBUtils){
        this.DBUtils = DBUtils
    }
    query(table,knex) {
        return this.DBUtils.getLatestTime(table,'sys_created_on').then(function(val) {
            // console.log('ORDERBYsys_created_on^ORDERBYsys_id^sys_created_on>' + val)
            return 'ORDERBYsys_created_on^ORDERBYsys_id^sys_created_on>' + val
        })
    }
    dbFunction(table,records,knex) {
        console.log('importing '+ records.length + ' newly created records')
        return this.DBUtils.knex.batchInsert(table, records, 10)
    }
}
module.exports = importCreated
