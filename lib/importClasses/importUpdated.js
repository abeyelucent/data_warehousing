var Promise = require('bluebird');
class importCreated {
    constructor(DBUtils) {
        this.DBUtils = DBUtils
    }
    query(table, knex) {
        return Promise.all([this.DBUtils.getLatestTime(table, 'sys_created_on'), this.DBUtils.getLatestTime(table, 'sys_updated_on')]).then((times) => {
            var query = 'ORDERBYsys_updated_on^ORDERBYsys_id^sys_updated_on>' + times[1] + '^sys_created_on<' + times[0]
            // console.log(query)
            return query
        })
    }
    dbFunction(table, records) {
        console.log(records.length)
        return this.DBUtils.knex.transaction((trx) => {
            return Promise.mapSeries(records, (record) => {
                return this.DBUtils.knex(table).where('sys_id', '=', record.sys_id).update(record).transacting(trx)
            })
        })
    }
}
module.exports = importCreated
