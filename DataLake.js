require('./encryption')
var Promise = require('bluebird')
class DataLake {
    constructor() {
        var db = require('./lib/utils/DBUtils');
        this.DBUtils = new db();
        this.SNUtils = require('./lib/utils/SNUtils');
        this.importRecords = require('./lib/importRecords');
        this.ETLUtils = require('./lib/utils/ETLUtils')
    }
    createSchema() {
        return this.DBUtils.createSchema()
    }
    importCreated(table) {
        var importCreated = require('./lib/importClasses/importCreated')
        return this.importRecords(table, new importCreated(this.DBUtils))
    }
    importUpdated(table) {
        var importUpdated = require('./lib/importClasses/importUpdated')
        return this.importRecords(table, new importUpdated(this.DBUtils), this.DBUtils.knex)
    }
    incrementalLoad(table) {
        return this.importUpdated(table).then(() => {
            return this.importCreated(table)
        }).then(() => {
            return this.incrementalDelete(table)
        })
    }
    dropTable(table) {
        return this.DBUtils.dropTable(table)
        // .then(() => {
        //     console.log(table + ' dropped')
        //     return
        // })
    }
    dropAllTables() {
        return this.DBUtils.dropAllTables()
    }
    createTable(table) {
        return this.DBUtils.createTable(table)
    }
    deleteAllRecords(table) {
        return this.DBUtils.deleteAllRecords(table)
    }
    syncTableSchema(table) {
        return this.DBUtils.syncTableSchema(table);
    }
    resync(table) {
        return this.deleteRecords(table).then(() => {
            return this.importAllRecords(table);
        })
    }
    importAllRecords(table) {
        var importAll = require('./lib/importClasses/importAll')
        return this.importRecords(table, new importAll(), this.DBUtils.knex)
    }
    fullImport() {
        require('./lib/fullImport')
    }
    getConfig() {
        var config = require('./config.json')
        console.log(`Instance: ${config.servicenow.instance}
Rows Per Request: ${config.servicenow.rowlimit}
Database: ${config.servicenow.instance}`)
        return Promise.resolve('')
    }
    getDBTableRowCount(table) {
        return this.DBUtils.getRowCount(table).then((val) => console.log(val))
    }
    getSNTableRowCount(table) {
        return this.SNUtils.getRowCount(table).then((val) => console.log(val));
    }
    _compareRowCount(table) {
        return Promise.props({
            sn: this.SNUtils.getRowCount(table),
            db: this.DBUtils.getRowCount(table)
        })
    }
    compareRowCount(table) {
        return this._compareRowCount(table).then((val) => {
            console.log(table + ',' + val.db + ',' + val.sn)
            return val
        })
    }
    updateRowCount(table) {
        return this._compareRowCount(table).then((counts) => {
            console.log('Updating row counts')
            return this.SNUtils.updateRowCount(table, {
                u_db_row_count: counts.db,
                u_sn_row_count: counts.sn
            })
        })
    }
    compareAllRowCounts() {
        return this.SNUtils.getImportTables().then((tables) => {
            var arr = []
            tables.forEach((table) => {
                arr.push(this.compareRowCount(table.table).catch((val) => {
                    return
                }))
            })
            return Promise.all(arr)
        })
    }
    incrementalDelete(table) {
        return this.DBUtils.incrementalDelete(table)
    }
}
module.exports = DataLake
